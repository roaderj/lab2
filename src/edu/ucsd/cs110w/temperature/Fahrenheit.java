/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author holi
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}
	public String toString() {
		// TODO: Complete this method
		return "" + getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float value = (getValue()-32) * 5/9;
		Temperature convert = new Celsius(value);
		return convert;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature convert = new Fahrenheit(getValue());
		return convert;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float value = (getValue()-32) * 5/9 + 273;
		Temperature convert = new Kelvin(value);
		return convert;
	}
}
