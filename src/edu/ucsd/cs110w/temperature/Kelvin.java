/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (holi): write class javadoc
 * @author holi
 *
 */
public class Kelvin extends Temperature {
	public Kelvin(float t) {
		super(t);
	}
	public String toString() {
		// TODO: Complete this method
		return "" + getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float value = getValue() - 273;
		Temperature convert = new Celsius(value);
		return convert;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float value = (getValue()-273) * 9/5 + 32;
		Temperature convert = new Fahrenheit(value);
		return convert;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Temperature convert = new Fahrenheit(getValue());
		return convert;
	}
}