/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author holi
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}
	public String toString() {
		// TODO: Complete this method
		return "" + getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature convert = new Celsius(getValue());
		return convert;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float value = getValue() * 9/5 + 32;
		Temperature convert = new Fahrenheit(value);
		return convert;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		float value = getValue() + 273;
		Temperature convert = new Kelvin(value);
		return convert;
	}
}
